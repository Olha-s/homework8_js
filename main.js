let form = document.querySelector('form');
let input = document.querySelector('input');

let container = document.createElement('div');
let span = document.createElement('span');
let button = document.createElement('button');
button.innerHTML= "&times";
button.classList.add('close');

let span2 = document.createElement('span');

form.after(span2);
container.prepend(span);
container.append(button );
form.before(container);
container.classList.add('container');

function changeInput () {
    input.classList.toggle('change');
}

function creatContainer() {
    if (Number(`${input.value}`) > 0) {
        span2.innerHTML = " ";
        input.classList.remove('change2');
        container.classList.add('change-div');
        span.innerHTML = `Текущая цена: ${input.value} $`;
        }
}

function creatSpan2 (){
    if(Number(`${input.value}`) <= 0) {
        input.classList.add('change2');
        span2.innerHTML = 'Please enter correct price';
        span2.classList.add('error');
        }
}

function onButtonClick (){
    input.value = "0";
    container.classList.remove('change-div');
    }

input.addEventListener('focus', changeInput);
input.addEventListener('blur', creatContainer);
input.addEventListener('blur', creatSpan2);
button.addEventListener('click', onButtonClick);

